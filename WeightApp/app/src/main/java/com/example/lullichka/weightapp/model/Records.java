package com.example.lullichka.weightapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lullichka on 23.09.16.
 */
public class Records {
    @SerializedName("records")
    @Expose
    private List<Record> records = new ArrayList<>();

    /**
     *
     * @return
     * The records
     */
    public List<Record> getRecords() {
        return records;
    }

    /**
     *
     * @param records
     * The records
     */
    public void setRecords(List<Record> records) {
        this.records = records;
    }

}
