package com.example.lullichka.weightapp.recorddetail;

import android.content.Context;

import com.example.lullichka.weightapp.model.Record;

/**
 * Created by lullichka on 03.10.16.
 */
public interface RecordDetailContract {
    interface View {
        void showName(String name);

        void showEmail(String email);

        void showPhone(String phone);
    }

    interface UserActionsListener {
        void openRecord(Integer recordId);

        void updateRecord(Context context, Record record);
    }
}
