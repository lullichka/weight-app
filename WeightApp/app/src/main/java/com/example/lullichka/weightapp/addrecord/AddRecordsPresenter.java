package com.example.lullichka.weightapp.addrecord;

import android.content.Context;

import com.example.lullichka.weightapp.data.RecordRepository;
import com.example.lullichka.weightapp.model.Record;

/**
 * Created by lullichka on 04.10.16.
 */
public class AddRecordsPresenter implements AddRecordContract.UserActionsListener {

    private final RecordRepository mRecordRepository;
    private final AddRecordContract.View mAddRecordView;

    public AddRecordsPresenter (RecordRepository recordRepository, AddRecordContract.View addRecordsView){
        mRecordRepository = recordRepository;
        mAddRecordView = addRecordsView;
    }
    @Override
    public void addRecord(Context context, String name, String email, String phone) {
        Record newRecord = new Record(name, email, phone);
        if (newRecord.isEmpty()){
            mAddRecordView.showEmptyRecordError();
        } else {
            mRecordRepository.addRecord(context, newRecord);
            mAddRecordView.showRecordsList();
        }
    }

    @Override
    public void updateRecord(Context context, Integer id, String name, String email, String phone) {
        Record updatedRecord = new Record(id, name, email, phone);
        if (updatedRecord.isEmpty()){
            mAddRecordView.showEmptyRecordError();
        } else {
            mRecordRepository.updateRecord(context, updatedRecord);
            mAddRecordView.showRecordsList();
        }
    }

}
