package com.example.lullichka.weightapp.recorddetail;

import android.content.Context;
import android.util.Log;

import com.example.lullichka.weightapp.data.RecordRepoInterface;
import com.example.lullichka.weightapp.model.Record;

/**
 * Created by lullichka on 03.10.16.
 */
public class RecordDetailPresenter implements RecordDetailContract.UserActionsListener {

    private static final String LOG_TAG = RecordDetailPresenter.class.getSimpleName();
    private final RecordRepoInterface mRecordRepository;
    private final RecordDetailContract.View mRecordDetailView;

    RecordDetailPresenter(RecordRepoInterface recordRepository,
                          RecordDetailContract.View recordDetailView) {
        mRecordRepository = recordRepository;
        mRecordDetailView = recordDetailView;
    }

    @Override
    public void openRecord(Integer recordId) {
        if (recordId == 0) {
            Log.v(LOG_TAG, "Record is missing");
            return;
        }
        mRecordRepository.getRecord(recordId, new RecordRepoInterface.GetRecordCallback() {
            @Override
            public void onRecordLoaded(Record record) {
                showRecord(record);
            }
        });
    }

    @Override
    public void updateRecord(Context context, Record record) {

    }
    private void showRecord(Record record) {
        String name = record.getName();
        String email = record.getEmail();
        String phone = record.getPhone();
        mRecordDetailView.showName(name);
        mRecordDetailView.showEmail(email);
        mRecordDetailView.showPhone(phone);
    }
}
