package com.example.lullichka.weightapp;

import android.app.Application;
import android.util.Log;

import com.facebook.stetho.Stetho;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by lullichka on 19.09.16.
 */
public class WeightApp extends Application {
    private static final String LOG_TAG = WeightApp.class.getSimpleName();

    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext())
                .name("database")
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        Log.v(LOG_TAG, "database created");
    }
}
