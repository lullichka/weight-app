package com.example.lullichka.weightapp.data;

import android.content.Context;
import android.util.Log;

import com.example.lullichka.weightapp.model.Record;
import com.example.lullichka.weightapp.model.Records;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lullichka on 03.10.16.
 */
public class RecordRepository implements RecordRepoInterface {

    private static final String LOG_TAG = RecordRepository.class.getSimpleName();
    private static final String BASE_URL = "http://cbook.cbcmrb.com/";
    private Realm mRealm;

    @Override
    public void getRecords(Context context, final LoadRecordsCallback callback) {
        mRealm = Realm.getDefaultInstance();
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(),
                        new SharedPrefsCookiePersistor(context));

        Retrofit retro = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                        new OkHttpClient.Builder()
                                .cookieJar(cookieJar)
                                .addNetworkInterceptor(new StethoInterceptor())
                                .build())
                .build();
        APIInterface retrofitRest = retro.create(APIInterface.class);
        Call<Records> call = retrofitRest.getContacts();
        call.enqueue(new Callback<Records>() {
            @Override
            public void onResponse(Call<Records> call, Response<Records> response) {
                mRealm = Realm.getDefaultInstance();
                List<Record> listRec = response.body().getRecords();
                List<Record> undeleted = new ArrayList<>();
                for (Record record : listRec) {
                    if (record.getIsDeleted() == 0) {
                        undeleted.add(record);
                    }
                }
                mRealm.beginTransaction();
                mRealm.copyToRealmOrUpdate(undeleted);
                mRealm.commitTransaction();
                callback.onRecordsLoaded(undeleted);
                Log.v(LOG_TAG, "loaded " + listRec.size());
            }
            @Override
            public void onFailure(Call<Records> call, Throwable t) {
                Log.v(LOG_TAG, t.getMessage());
            }
        });
        mRealm.close();
    }

    @Override
    public void getRecord(Integer recordId, GetRecordCallback callback) {
        mRealm = Realm.getDefaultInstance();
        RealmQuery<Record> query = mRealm.where(Record.class);
        query.equalTo("id", recordId);
        Record record = query.findFirst();
        mRealm.close();
        callback.onRecordLoaded(record);
    }

    @Override
    public void addRecord(Context context, final Record record) {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(),
                        new SharedPrefsCookiePersistor(context));

        Retrofit retro = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                        new OkHttpClient.Builder()
                                .cookieJar(cookieJar)
                                .addNetworkInterceptor(new StethoInterceptor())
                                .build())
                .build();
        APIInterface retrofitRest = retro.create(APIInterface.class);
        Call<Record> call = retrofitRest.saveContact(record);
        call.enqueue(new Callback<Record>() {
            @Override
            public void onResponse(Call<Record> call, Response<Record> response) {
                Log.v(LOG_TAG, "Successfully added " + String.valueOf(record.getName()));
            }

            @Override
            public void onFailure(Call<Record> call, Throwable t) {
                Log.v(LOG_TAG, t.getMessage().toString());
            }
        });
    }

    @Override
    public void deleteRecord(Context context, final Record record ) {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(),
                        new SharedPrefsCookiePersistor(context));

        Retrofit retro = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                        new OkHttpClient.Builder()
                                .cookieJar(cookieJar)
                                .addNetworkInterceptor(new StethoInterceptor())
                                .build())
                .build();
        APIInterface retrofitRest = retro.create(APIInterface.class);
        Call<String> call = retrofitRest.deleteContact(record);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.v(LOG_TAG, "Successfully deleted " + String.valueOf(record.getName()));

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.v(LOG_TAG, "Delete failed "+t.getMessage().toString());
            }
        });
    }

    @Override
    public void updateRecord(Context context, final Record record) {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(),
                        new SharedPrefsCookiePersistor(context));

        Retrofit retro = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                        new OkHttpClient.Builder()
                                .cookieJar(cookieJar)
                                .addNetworkInterceptor(new StethoInterceptor())
                                .build())
                .build();
        APIInterface retrofitRest = retro.create(APIInterface.class);
        Call<Integer> call = retrofitRest.updateContact(record);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                Log.v(LOG_TAG, "Successfully updated " + String.valueOf(record.getName()));
                mRealm = Realm.getDefaultInstance();
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmQuery<Record> query = mRealm.where(Record.class);
                        query.equalTo("id", record.getId());
                        Record recordUpdate = query.findFirst();
                        recordUpdate.setName(record.getName());
                        recordUpdate.setEmail(record.getEmail());
                        recordUpdate.setPhone(record.getPhone());
                    }
                });
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.v(LOG_TAG, t.getMessage().toString());
            }
        });
    }


}
