package com.example.lullichka.weightapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lullichka on 22.09.16.
 */
public class UserLogin {

    @SerializedName("password")
    private String password;
    @SerializedName("username")
    private String username;

}