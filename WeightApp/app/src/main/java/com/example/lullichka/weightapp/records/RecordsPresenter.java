package com.example.lullichka.weightapp.records;

import android.content.Context;
import android.util.Log;

import com.example.lullichka.weightapp.data.RecordRepoInterface;
import com.example.lullichka.weightapp.data.RecordRepository;
import com.example.lullichka.weightapp.model.Record;

import java.util.List;

/**
 * Created by lullichka on 03.10.16.
 */
public class RecordsPresenter implements RecordsContract.UserActionsListener {

    private static final String LOG_TAG = RecordsPresenter.class.getSimpleName();
    private final RecordRepository mRecordRepository;
    private final RecordsContract.View mRecordsView;


    public RecordsPresenter(RecordRepository recordRepository, RecordsContract.View recordsView){
        mRecordRepository = recordRepository;
        mRecordsView = recordsView;
    }
    @Override
    public void loadRecords(Context context) {
        mRecordRepository.getRecords(context, new RecordRepoInterface.LoadRecordsCallback() {
            @Override
            public void onRecordsLoaded(List<Record> records) {
                mRecordsView.showRecords(records);
            }
        });
    }

    @Override
    public void deleteRecord(Context context, Record record) {
        mRecordRepository.deleteRecord(context, record);
        Log.v(LOG_TAG, "delete "+record.getId());
    }

    @Override
    public void openRecordDetails(Record record) {
        mRecordsView.showRecordDetail(record.getId());
    }
}
