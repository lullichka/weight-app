package com.example.lullichka.weightapp.recorddetail;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.lullichka.weightapp.R;
import com.example.lullichka.weightapp.addrecord.AddRecordFragment;

public class DetailActivity extends AppCompatActivity {


    private static final String LOG_TAG = DetailActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final DetailFragment detailFragment = new DetailFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentFrame, detailFragment).commit();
        final FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        if (floatingActionButton != null) {
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = detailFragment.getTextViewName().getText().toString();
                    String email = detailFragment.getTextViewEmail().getText().toString();
                    String phone = detailFragment.getTextViewPhone().getText().toString();
                    int id = detailFragment.getRecordId();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentFrame, AddRecordFragment.newInstance(id, name, email, phone))
                            .commit();
                    Log.v(LOG_TAG, "id is "+String.valueOf(id));
                    floatingActionButton.setImageResource(R.drawable.ic_done_white_24dp);
                }
            });
        }
    }
}
