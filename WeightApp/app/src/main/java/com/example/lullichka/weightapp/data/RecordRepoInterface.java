package com.example.lullichka.weightapp.data;

import android.content.Context;

import com.example.lullichka.weightapp.model.Record;

import java.util.List;

/**
 * Created by lullichka on 03.10.16.
 */
public interface RecordRepoInterface {
    interface LoadRecordsCallback {
        void onRecordsLoaded(List<Record> records);
    }

    interface GetRecordCallback {
        void onRecordLoaded(Record record);
    }

    void getRecords(Context context, LoadRecordsCallback callback);

    void getRecord(Integer recordId, GetRecordCallback callback);

    void addRecord(Context context, Record record);

    void deleteRecord(Context context, Record record);

    void updateRecord(Context context, Record record);

}
