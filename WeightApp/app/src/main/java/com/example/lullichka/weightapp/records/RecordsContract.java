package com.example.lullichka.weightapp.records;

import android.content.Context;

import com.example.lullichka.weightapp.model.Record;

import java.util.List;

/**
 * Created by lullichka on 03.10.16.
 */
public interface RecordsContract {
    interface UserActionsListener {
        void loadRecords(Context context);

        void deleteRecord(Context context, Record record);

        void openRecordDetails(Record record);

    }

    interface View {
        void showRecords(List<Record> records);

        void showRecordDetail(Integer requestedRecordId);
    }
}
