package com.example.lullichka.weightapp.recorddetail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lullichka.weightapp.R;
import com.example.lullichka.weightapp.data.RecordRepository;

/**
 * Created by lullichka on 04.10.16.
 */
public class DetailFragment extends Fragment implements RecordDetailContract.View {
    public static final String EXTRA_RECORD_ID = "recordId";
    private static final String LOG_TAG = DetailFragment.class.getSimpleName();
    private TextView mTvName;
    private TextView mTvEmail;
    private TextView mTvPhone;
    private int mRecord_id;

    public DetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        mTvName = (TextView) rootView.findViewById(R.id.tv_Name);
        mTvEmail = (TextView) rootView.findViewById(R.id.tv_email);
        mTvPhone = (TextView) rootView.findViewById(R.id.tv_phone);


        final RecordDetailContract.UserActionsListener userActionsListener = new RecordDetailPresenter(
                new RecordRepository(), this);

        mRecord_id = getActivity().getIntent().getIntExtra(EXTRA_RECORD_ID, 0);

        userActionsListener.openRecord(mRecord_id);

        return rootView;
    }

    public TextView getTextViewName(){
        return mTvName;
    }
    public TextView getTextViewEmail(){
        return mTvEmail;
    }
    public TextView getTextViewPhone(){
        return mTvPhone;
    }
    public Integer getRecordId(){
        return mRecord_id;
    }

    @Override
    public void showName(String name) {
        mTvName.setText(name);
    }

    @Override
    public void showEmail(String email) {
        mTvEmail.setText(email);
    }

    @Override
    public void showPhone(String phone) {
        mTvPhone.setText(phone);
    }
}
