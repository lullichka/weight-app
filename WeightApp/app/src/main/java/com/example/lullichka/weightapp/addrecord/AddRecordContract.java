package com.example.lullichka.weightapp.addrecord;

import android.content.Context;

/**
 * Created by lullichka on 04.10.16.
 */
public interface AddRecordContract {
    interface UserActionsListener {
        void addRecord(Context context, String name, String email, String phone);

        void updateRecord(Context context, Integer id, String name, String email, String phone);
    }

    interface View {
        void showRecordsList();

        void showEmptyRecordError();
    }
}
