package com.example.lullichka.weightapp.data;

import com.example.lullichka.weightapp.model.Record;
import com.example.lullichka.weightapp.model.Records;
import com.example.lullichka.weightapp.model.UserLogin;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by lullichka on 21.09.16.
 */
public interface APIInterface {

    @GET("contacts.php?action=getContacts")
    Call<Records> getContacts();

    @FormUrlEncoded
    @POST("contacts.php?action=login")
    Call<UserLogin> login(
            @Field("username") String username,
            @Field("password") String password);

    @POST("contacts.php?action=addContact")
    Call<Record> saveContact(
            @Body Record recordToAdd);

    @POST("contacts.php?action=updateContact")
    Call<Integer> updateContact(
            @Body Record record);

    @POST("contacts.php?action=deleteContact")
    Call<String> deleteContact( //todo read about calls types
            @Body Record record);
}
